#coding: utf-8

#############################################
# Author: Michel Martinez - 28/08/2017      #
# you can test with:                        #
# python main.py "03/04/2011 15:40" - 1500  #
# or with any date, mins or operation       #
#############################################


#import only for take the args from user
import sys


def change_date(date, op, value):
    try:
        if op != '+' and op != '-':
            raise ValueError("op")

    except ValueError as error:
        if str(error) == "op":
            return("Invalid Operation")
        else:
            return("Value error: " + str(error))

    else:
        #months_days is a tuple with the number of days per month
        months_days = (31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31)

        #divide into date and time lists the informed string date
        times = date.split(' ')
        days = times[0].split('/')
        hours = times[1].split(':')
        
        #Transforms the year, day and hours into minutes and adds together the minutes informed
        timestemp_fake = int(hours[1]) + (int(hours[0])*60) + (int(days[0])*1440) + (int(days[2])*525600)

        #Adds the months transformed into minutes
        cont = 0
        while (int(days[1]) > cont) and cont < 12:
            timestemp_fake += months_days[cont]*1440
            cont += 1

        #make the operation in minutes ('elif' can be 'else' only)
        if op == '+':
            timestemp_fake += abs(value)
        elif op == '-':
            timestemp_fake -= abs(value)

        #return the fake timestemp to string format dd/MM/yyyy HH:mm
        final_year = str(int(timestemp_fake / 525600))
        timestemp_fake = timestemp_fake % 525600
        
        cont = 0
        while timestemp_fake > months_days[cont]*1440:
            timestemp_fake -= months_days[cont]*1440
            cont += 1
        
        if cont == 0:
            cont = 12
            final_year = str(int(final_year) - 1)

        if cont < 10:
            final_month = ("0" + str(cont))
        else:
            final_month= str(cont)
        
        final_day = str(int(timestemp_fake / 1440))
        timestemp_fake = timestemp_fake % 1440
        
        #test for 0 day, if + only add 1 if - needs make -1 in day(pass for the last day), month and year
        if final_day == "0":
            if op == "+":
                final_day = "1"
            else:
                final_day = str(months_days[cont-1])
                final_month = str(int(final_month) - 1)
                if final_month == "0":
                    final_year = str(int(final_year) - 1)

        final_hour = str(int(timestemp_fake / 60))
        timestemp_fake = timestemp_fake % 60
        
        #this is only for make the format keep in 2 digits
        if timestemp_fake < 10:
            final_min = ("0" + str(timestemp_fake))
        else:
            final_min = str(timestemp_fake)

        return (final_day + "/" + final_month + "/" + final_year + " " + final_hour + ":" + final_min)
        

if __name__ == '__main__':
    print(change_date(sys.argv[1], sys.argv[2], int(sys.argv[3])))